# Ejercicio TDD Lift

Basado en [The Lift Kata](https://kata-log.rocks/lift-kata).

Implementar usando TDD un objeto Lift (ascensor) que realice las siguientes operaciones:

1. Reciba un pedido de piso destino de un pasajero ya dentro del ascensor.
2. Reciba una llamada desde un piso origen junto con la dirección (arriba / abajo) de un pasajero fuera del ascensor.
3. Se mueva un piso en la dirección correcta dadas las siguientes condiciones para los pedidos de destino:
   1. Si hay pedidos de destino por pasajeros por arriba del piso actual, se mueve en la dirección del destino más cercano. ej: si el ascensor está en el piso 3 y tiene destinos en los pisos 5 y 7; debe moverse al piso 4, luego al 5, luego al 6 y luego al 7.
   2. Si hay pedidos de destino por pasajeros por debajo del piso actual, se mueve en la dirección del destino más cercano. ej: si el ascensor está en el piso 3 y tiene destinos en los pisos 2 y 1; debe moverse al piso 2 y luego al 1.
   3. Si hay pedidos de destino por pasajeros por arriba y por debajo del piso actual, debe darle prioridad a los destinos de los pisos más bajos. ej: si el ascensor está en el piso 3 y tiene destinos en los pisos 1, 2, 5 y 7; debe moverse al piso 2, luego al 1, y luego ascender de a un piso al 5 y 7 como en **1.**
   4. Para cualquiera de los anteriores, si pasa por un piso desde el que fue llamado y la dirección es la que tiene el ascensor actualmente, puede detenerse a subir al pasajero (eliminando la llamada).
   5. Si no hay pedidos de destino debe responder a las llamadas, acudiendo a la llamada desde el piso más alto sin importar su dirección.
   6. Si no hay pedidos de destino ni llamadas, y está en un piso superior al 0, debe volver de a un piso hasta la planta baja (piso 0).
   7. Si está en el piso 0 y no hay destinos ni llamadas, no debe moverse.
